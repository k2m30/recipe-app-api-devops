#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user


export LATEST_SSHCHAT=$(curl -s https://api.github.com/repos/shazow/ssh-chat/releases | grep -om1 "https://.*/ssh-chat-linux_amd64.tgz")
wget "${LATEST_SSHCHAT}"
sudo tar -xf ssh-chat-linux_amd64.tgz -C /opt
sudo ln -sf /opt/ssh-chat/ssh-chat /usr/local/bin/ssh-chat
sudo ssh-keygen -t rsa -N '' -f /root/.ssh/id_rsa
ssh-keygen -t rsa -N '' -f /home/ec2-user/.ssh/id_rsa
ssh-chat --admin=$HOME/.ssh/authorized_keys --bind=0.0.0.0:2022 &
disown
