variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "1m@tut.by"
}

variable "db_username" {
}

variable "db_password" {

}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}